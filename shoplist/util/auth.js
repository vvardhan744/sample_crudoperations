const jwt  = require('jsonwebtoken');
const response = require("./responseHandler");

module.exports = (req, res, next) => {
    try {
        const token = req.header('Authorization').replace('Bearer', '').trim();        
        const decoded  = jwt.verify(token, 'thisismynewblog');

        req.token = token
       
        next()

    } catch (error) {
        response.error(req, res,"Please authenticate",error)
    }


}