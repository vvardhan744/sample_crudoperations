const express = require("express");
const bodyParser = require('body-parser');
var guest = require("../cart/guest/guest.routes");
var item = require("../cart/item/item.routes");
module.exports = () => {
    const app = express();
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));
    //item(app);
    guest(app);
    return app;
}