module.exports = {
    TEST_PATH:"/",
    ITEM_CREATE:"/Create",
    GUEST_CREATE:"/Guest/Create",
    GUEST_UPDATE:"/Guest/Update",
    GUEST_DELETE:"/Guest/Delete"
}