const mongoose = require("mongoose");
var itemSchema = mongoose.Schema({
    itemName:{
        type:String  
    },
    price:{
        type:String  
    },
    details:{
        type:String
    }
})
module.exports = mongoose.model("item",itemSchema)