var itemModel = require("./item.model");
var message = require("../../config/messages.json");
var response = require("../../util/responseHandler");
var item = {};

item.signup = (req, res) => {
    itemModel.find({}).then(
        doc => {
            response.success(req, res, message.preview, doc)
        },
        err => {
            response.error(req, res, '', err)
        }
    )
}
item.createitem = (req, res) => {
    console.log(req.body);

    var itemdb = new itemModel(req.body);

    itemdb.save().then(
        doc => {
            response.success(req, res, message.created , doc)
        },
        err => {
            response.error(req, res, '', err)
        }
    )
}
 item.updateitem =(req,res) =>{
    var myquery = { itemName: req.body.itemName };
    var newvalues = { $set: { details: req.body.details} };
    itemModel.updateOne(myquery,newvalues).then(
        doc => {
            response.success(req, res, message.updated , doc)
        },
        err => {
            response.error(req, res, '', err)
        }
    )
       
    

 }
item.deleteitem =(req,res) =>{
    itemModel.deleteOne(req.body.itemName).then(
        doc => {
            response.success(req, res, message.deleted , doc)
        },
        err => {
            response.error(req, res, '', err)
        }
    )

}


module.exports = item;