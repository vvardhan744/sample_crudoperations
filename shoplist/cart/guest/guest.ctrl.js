var guestModel = require("./guest.model");
var message = require("../../config/messages.json");
var response = require("../../util/responseHandler");
var guest = {};

guest.signup = (req, res) => {
    guestModel.find({}).then(
        doc => {
            response.success(req, res, message.preview, doc)
        },
        err => {
            response.error(req, res, '', err)
        }
    )
}
guest.createguest = (req, res) => {
    console.log(req.body);

    var guestdb = new guestModel(req.body);

    guestdb.save().then(
        doc => {
            response.success(req, res, message.created , doc)
        },
        err => {
            response.error(req, res, '', err)
        }
    )
}
 guest.updateguest =(req,res) =>{
    var myquery = { guestName: req.body.guestName };
    var newvalues = { $set: { deliveryaddress: req.body.address , phonenumber: req.body.phonenumber } };
    guestModel.updateOne(myquery,newvalues).then(
        doc => {
            response.success(req, res, message.updated , doc)
        },
        err => {
            response.error(req, res, '', err)
        }
    )
       
    

 }
guest.deleteguest =(req,res) =>{
   guestModel.deleteOne(req.body.guestName).then(
        doc => {
            response.success(req, res, message.deleted , doc)
        },
        err => {
            response.error(req, res, '', err)
        }
    )

}


module.exports = guest;