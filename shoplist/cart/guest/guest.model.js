const mongoose = require("mongoose");
var guestSchema = mongoose.Schema({
    guestName:{
        type:String  
    },
    password:{
        type:String  
    },
    phonenumber:{
        type:Number
    },
    deliveryaddress:{
        type:String
    }
})
module.exports = mongoose.model("guest",guestSchema)