const  routes = require("../../config/routes");
const guest = require("./guest.ctrl")


module.exports = (app) =>{
    app.get(routes.TEST_PATH, guest.signup);
    app.post(routes.GUEST_CREATE,guest.createguest);
    app.post(routes.GUEST_UPDATE, guest.updateguest);
    app.post(routes.GUEST_DELETE,guest.deleteguest);

}