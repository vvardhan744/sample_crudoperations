
const  routes = require("../../config/routes");
const admin = require("./admin.ctrl")


module.exports = (app) =>{
    app.get(routes.TEST_PATH, admin.signup);
    app.post(routes.ADMIN_CREATE,admin.createadmin);
    app.post(routes.ADMIN_UPDATE, admin.updateadmin);
    app.post(routes.ADMIN_DELETE,admin.deleteadmin);

}