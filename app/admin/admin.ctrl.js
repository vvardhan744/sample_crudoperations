var adminModel = require("./admin.model");
var message = require("../../config/messages.json");
var response = require("../../util/responseHandler");
var admin = {};

admin.signup = (req, res) => {
    adminModel.find({}).then(
        doc => {
            response.success(req, res, message.preview, doc)
        },
        err => {
            response.error(req, res, '', err)
        }
    )
}
admin.createadmin = (req, res) => {
    console.log(req.body);

    var admindb = new adminModel(req.body);

    admindb.save().then(
        doc => {
            response.success(req, res, message.created , doc)
        },
        err => {
            response.error(req, res, '', err)
        }
    )
}
 admin.updateadmin =(req,res) =>{
    var myquery = { UserName: req.body.UserName };
    var newvalues = { $set: { address: req.body.address , phonemumber: req.body.phonemumber } };
    adminModel.updateOne(myquery,newvalues).then(
        doc => {
            response.success(req, res, message.updated , doc)
        },
        err => {
            response.error(req, res, '', err)
        }
    )
       
    

 }
admin.deleteadmin =(req,res) =>{
    //var myquery = new adminModel(req.body);
    adminModel.deleteOne(req.body.UserName).then(
        doc => {
            response.success(req, res, message.deleted , doc)
        },
        err => {
            response.error(req, res, '', err)
        }
    )

}


module.exports = admin;