
const  routes = require("../../config/routes");
const user = require("./user.ctrl")


module.exports = (app) =>{
    app.get(routes.TEST_PATH, user.signup);
    app.post(routes.USER_CREATE,user.createuser);

}