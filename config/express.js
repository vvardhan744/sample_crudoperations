const express = require("express");
const bodyParser = require('body-parser');
var user = require("../app/user/user.routes");
var admin = require("../app/admin/admin.routes");
module.exports = () => {
    const app = express();
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));
    //user(app);
    admin(app);
    return app;

}