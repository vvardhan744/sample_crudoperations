
const mongoose = require("mongoose");
const config = require("./config")


module.exports = () => {
    mongoose.connect(config.mongoose_connection_url, { useNewUrlParser: true }).then(
        doc => {
            console.log("DB connected");
        },
        err => {
            console.log("db connect error ,error is ", err.message);
        }
    )
}