module.exports = {
    TEST_PATH:"/",
    USER_CREATE:"/Create",
    ADMIN_CREATE:"/Admin/Create",
    ADMIN_UPDATE:"/Admin/Update",
    ADMIN_DELETE:"/Admin/Delete"
}