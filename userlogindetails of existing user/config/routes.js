module.exports = {
    TEST_PATH:"/",
    USER_CREATE:"/User/Create",
    USER_UPDATE:"/User/Update",
    USER_DELETE:"/User/Delete",
    USER_LOGIN:"/User/Login"
}