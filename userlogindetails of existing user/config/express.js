const express = require("express");
const bodyParser = require('body-parser');
var user = require("../vegawallet/user/user.routes");
module.exports = () => {
    const app = express();
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));
    user(app);
    return app;
}