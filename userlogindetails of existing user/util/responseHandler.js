module.exports.success = (req,res,msg,data) =>{

    res.json({
        Status:200,
        Data:data,
        Message:msg
    })
}

module.exports.error = (req,res,msg,error) =>{

    res.json({
        Status:400,
        Message:msg || error.message
    })
}