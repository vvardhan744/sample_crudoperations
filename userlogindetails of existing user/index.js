process.env.NODE_ENV = process.env.NODE_ENV || "development";
console.log(process.env.NODE_ENV)
const config = require("./config/config");
const express = require("./config/express");
const db = require("./config/db");
db();
const app = express();
app.listen(config.port, () => {
    console.log(`app listening on port ${config.port}!`)
})
