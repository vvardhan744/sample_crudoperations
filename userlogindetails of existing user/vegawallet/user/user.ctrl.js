var userModel = require("./user.model");
var message = require("../../config/messages.json");
var response = require("../../util/responseHandler");
var user = {};

user.signup = (req, res) => {
    userModel.find({}).then(
        doc => {
            response.success(req, res, message.preview, doc)
        },
        err => {
            response.error(req, res, '', err)
        }
    )
}
user.createuser = (req, res) => {
    console.log(req.body);

    var userdb = new userModel(req.body);

    userdb.save().then(
        doc => {
            response.success(req, res, message.created, doc)
        },
        err => {
            response.error(req, res, '', err)
        }
    )
}
user.updateuser = (req, res) => {
    var myquery = { userName: req.body.userName };
    var newvalues = { $set: { email: req.body.email, phonenumber: req.body.phonenumber } };
    userModel.updateOne(myquery, newvalues).then(
        doc => {
            response.success(req, res, message.updated, doc)
        },
        err => {
            response.error(req, res, '', err)
        }
    )



}
user.deleteuser = (req, res) => {
    var myquery = new userModel(req.body);
    userModel.deleteOne(myquery).then(
        doc => {
            response.success(req, res, message.deleted, doc)
        },
        err => {
            response.error(req, res, '', err)
        }
    )
}



user.loginuser = (req, res) => {

    if (req.body.userName && req.body.password) {
    }
    else {
        res.json({
            code: 204,
            message: "Please user name and password"
        });
        return false;
    }

    let loginData = {
        userName: req.body.userName,
        password: req.body.password
    };
    console.log(loginData);
    try {

        userModel.findOne(loginData, { password: 0 }, (err, result) => {
            if (err) {
                response.error(req, res, '', err)
            }
            else {
                console.log("login result:", result);
                if (result) {
                    req.body = result;

                    res.json({
                        code: 200,
                        message: "success",
                        data: result
                    });
                }
            }

        });

    }
    catch (err) {
        console.log(err);
        response.error(req, res, '', err)
    }

}









module.exports = user;