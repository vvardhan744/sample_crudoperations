const mongoose = require("mongoose");
var userSchema = mongoose.Schema({
    userName:{
        type:String  
    },
    firstName:{
        type:String
    },
    lastName:{
        type:String
    },
    email:{
        type:String  
    },
    password:{
        type:String
    }
})
module.exports = mongoose.model("user",userSchema)