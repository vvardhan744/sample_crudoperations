const  routes = require("../../config/routes");
const user = require("./user.ctrl")


module.exports = (app) =>{
    app.get(routes.TEST_PATH, user.signup);
    app.post(routes.USER_CREATE,user.createuser);
    app.post(routes.USER_UPDATE, user.updateuser);
    app.post(routes.USER_DELETE,user.deleteuser);
    app.post(routes.USER_LOGIN,user.loginuser);

}